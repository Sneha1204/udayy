 ## Algorithm for creating Options

 **Step 1** : Calculate the number of       digits for the correct answer recieved after subtracting the subtrahend from the minuend

 **Step 2** : Perform Math.pow(10,numOfDigits -1) and save it as gap variable. This ensures that if the correct answer is 20, we get 10 as the gap.

 **Step 3** : We can get the first three options including the correct answer by simply adding and subtracting the correct answer with gap which ensures its not completely random and also that the last few digits of the answer remains same thus drawing a similarity between the options.
 **Ex** - if our correct answer is 20, our options will be 10,20,30 after subtracting and adding the gap to correct answer respectively.

 **Step 4** : To generate the fourth option, we break the logic into two steps.
* If the correct answer is of a single digit, we simply add the first three options resulting in 3 * correctAnswer

* If the correct answer is more than a single digit we perform **Math.ceil(correctAnswer + (Math.random() * Math.pow(10,numOfDigits-1)))**. This generates a random number and also adds the correct answer to the random number thus ensuring a close proximity to the correct answer.

 