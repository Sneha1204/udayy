FROM node:alpine
WORKDIR /Udayy
COPY package.json /Udayy
RUN npm install
COPY . /Udayy
CMD ["npm","start"]  