# README
### What is this repository for?

* This repository contains all **API** related code under the **routes** folder
* The document explaining the logic behind options is written in optionsDocument.md
* The document explaining the design schema is written in designSchema.md



## Git Clone
-----
Run the following command in your local machine to download the entire code.

```
git clone https://Sneha1204@bitbucket.org/Sneha1204/udayy.git
```
## Installation
----
Run the following command to install all the packages in *package.json*

```
npm install
```

## Start the Development Server
----
Run the following command to start the server
```
npm start
```
## Start the Test Server
----
Run the following command to start the test server
```
npm test
```
## Docker
---
You can download the code from the repository **d1204/udayy0208** in dockerhub.

## Format of Input data
---
The format of the request body I have used based on which the code is written is 

```
{
    "data" : {
    "numOfQuestions" : 3,
    "info" : [
        {
            "question" : 1,
            "minuendDigits" : 1,
            "subtrahendDigits" : 1,
            "flag" : false
        }
        ,
        {
            "question" : 2,
            "minuendDigits" : 4,
            "subtrahendDigits" : 3,
            "flag" : true
        },
        {
            "question" : 3,
            "minuendDigits" : 6,
            "subtrahendDigits" : 4,
            "flag" : true
        }
    ]
}
}
```
## Response format of data
---

This will return a response in the following format :

```
{
    "response": [
        {
            "question": 1,
            "minuend": 9,
            "subtrahend": 5,
            "correctAnswer": 4,
            "options": [
                5,
                4,
                3,
                12
            ]
        },
        {
            "question": 2,
            "minuend": 8051,
            "subtrahend": 473,
            "correctAnswer": 7578,
            "options": [
                8578,
                7578,
                6578,
                8524
            ]
        },
        {
            "question": 3,
            "minuend": 761499,
            "subtrahend": 6050,
            "correctAnswer": 755449,
            "options": [
                855449,
                755449,
                655449,
                783220
            ]
        }
    ]
}
```