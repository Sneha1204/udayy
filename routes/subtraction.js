const express = require('express')
const router = express.Router()

router.post('/',async(req,res) =>{
    const data = req.body.data
    let allPromises = []
    data.info.forEach(async each =>{
        let question = each.question
        let minuendDigits = each.minuendDigits
        let subtrahendDigits = each.subtrahendDigits
        let flag = each.flag
        if(minuendDigits < subtrahendDigits){
            res.status(400).json({ error : 'Number of digits in minuend cannot be less than subtrahend'})
        }
        else{
            allPromises.push(calculate(question,minuendDigits,subtrahendDigits,flag))
        }
    })

    Promise.all(allPromises).then(promiseRes =>{
        promiseRes.sort((a,b) => (a.question > b.question) ? 1 : ((b.question > a.question) ? -1 : 0))
        res.status(200).json({response : promiseRes})
    }).catch(err =>{
        console.log("Err ",err)
        res.status(500).json({error : err})
    })
})


  function calculate(question,minuendDigits,subtrahendDigits,flag){
     return new Promise(async(resolve,reject)=>{
        const minuendNumber = Math.ceil(Math.random()* Math.pow(10,minuendDigits))
        const subtrahendNumber = Math.ceil(Math.random()* Math.pow(10,subtrahendDigits))
        const minuendNumOfDigits = await getLength(minuendNumber)
        const subtrahendNumOfDigits = await getLength(subtrahendNumber)
        
        let paddedSubtrahendNum

        //ensuring the generated minuend and subtrahend is not less than the number of digits entered by the user
        if(minuendNumOfDigits < minuendDigits || subtrahendNumOfDigits < subtrahendDigits){
            return resolve(calculate(question,minuendDigits,subtrahendDigits,flag))
        }

        //ensuring equal number of digits
        if(subtrahendNumOfDigits < minuendNumOfDigits){
            paddedSubtrahendNum = await padNumber(subtrahendNumber,minuendNumOfDigits)
        }
       
        //ensuring that the subtraction result is not negative
        if(minuendNumber > subtrahendNumber)
        {
            
            let minuendArr = minuendNumber.toString().split('')
            let subtrahendArr = subtrahendNumOfDigits < minuendNumOfDigits ? paddedSubtrahendNum.split('') : subtrahendNumber.toString().split('')
            let count = 0
        
            // borrowing must happen since flag is true
            if(flag){
                for(let i = 0 ; i< minuendArr.length; i++){
                    //checking if corresponding digits in minuend is less than subtrahend
                    if(minuendArr[i] < subtrahendArr[i])
                    {
                        count = count + 1
                        break
                    }
                }
                //if no borrowing happens minuend and subtrahend are re-generated
                if(count < 1){
                    return resolve(calculate(question,minuendDigits,subtrahendDigits,flag))
                }
            }

            //borrowing should not happen since flag is false
            else{
                for(let i = 0 ; i< minuendArr.length; i++){
                    if(minuendArr[i] < subtrahendArr[i])
                    {
                        count = count + 1
                        break
                    }
                }
                if(count === 1){
                    
                    return resolve(calculate(question,minuendDigits,subtrahendDigits,flag))
                }
            }
            const correctAnswer = minuendNumber - subtrahendNumber
            
            let options = []
            let numOfDigits = await getLength(correctAnswer)
            let gap = Math.pow(10,numOfDigits-1)
            let fourthOption = await generateOption(gap,correctAnswer, numOfDigits)
            options = [...options,correctAnswer + gap, correctAnswer, correctAnswer - gap, fourthOption]
            
            let responseObj = {}
            responseObj.question = question
            responseObj.minuend = minuendNumber
            responseObj.subtrahend = subtrahendNumber
            responseObj.correctAnswer = correctAnswer
            responseObj.options = options
            resolve(responseObj)
        }
        else{
            
            return resolve(calculate(question,minuendDigits,subtrahendDigits,flag))
        }
     })
        
}

async function generateOption(gap,correctAnswer,numOfDigits){
    let fOption 
    if(numOfDigits === 1){
        fOption = 3 * correctAnswer
    }
    else{
        fOption = Math.ceil(correctAnswer + (Math.random() * Math.pow(10,numOfDigits-1)))
        // console.log("foption in func is ",fOption)
        
    }
    return fOption
}

async function getLength(number){
    return number.toString().length
}

async function padNumber(num, size) {
    num = num.toString()
    while (num.length < size) {
        num = "0" + num
    }
    return num
}
module.exports = router