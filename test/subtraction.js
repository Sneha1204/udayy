let chai = require('chai')
let chaiHttp = require('chai-Http')

chai.use(chaiHttp)
chai.should()

const baseUrl = 'http://localhost:8080'

describe('GET /subtraction',()=>{
    
    it('it should thow error code 400 if minuend digits are more than subtrahend digits', async()=>{
        try{
            const errParams = {
                "data" : {
                "numOfQuestions" : 1,
                "info" : [
                    {
                        "question" : 1,
                        "minuendDigits" : 1,
                        "subtrahendDigits" : 2,
                        "flag" : false
                    }
                ]
            }
            }
            const response = await chai.request(baseUrl)
            .post('/subtraction')
            .send(errParams)
            response.should.have.status(400)
            response.should.have.property('error')
        }
        catch(error){
            console.log("Error is ",error)
        }
    })

    const params = {
        "data" : {
        "numOfQuestions" : 1,
        "info" : [
            {
                "question" : 1,
                "minuendDigits" : 1,
                "subtrahendDigits" : 1,
                "flag" : false
            }
            ,
            {
                "question" : 2,
                "minuendDigits" : 4,
                "subtrahendDigits" : 3,
                "flag" : true
            },
            {
                "question" : 3,
                "minuendDigits" : 6,
                "subtrahendDigits" : 4,
                "flag" : true
            }
        ]
        }
    }

    it('it should respond with status code 200 with question,minuend,subtrahend,correctAnswer,options as its keys and options key should have a length of 4', async()=>{
        try{
            const response = await chai.request(baseUrl)
            .post('/subtraction')
            .send(params)
            response.should.have.status(200)
            response.body.response.forEach(each =>{
                each.should.have.all.keys('question','minuend','subtrahend','correctAnswer','options')
            })
            response.body.response.forEach(each =>{
                each.options.should.have.lengthOf(4)
            })
        }
        catch(error){
            console.log("Error is ",error)
        }
    })

    it('it should not have minuend, subtrahend,correct answer or any of the values in the options array as negative', async()=>{
        try{
            const response = await chai.request(baseUrl)
            .post('/subtraction')
            .send(params)
            response.should.have.status(200)
            response.body.response.forEach(each =>{
                each.minuend.should.be.above(0)
                each.subtrahend.should.be.above(0)
                each.correctAnswer.should.be.above(0)
                each.options.forEach(el => el.should.be.above(0))
            })
        }
        catch(error){
            console.log("Error is ",error)
        }
    })

})