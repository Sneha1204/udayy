const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended : true}))
app.use(cors())

const sub = require('./routes/subtraction')
const port = process.env.port || 8080

app.use('/subtraction', sub)

app.listen(port, ()=>{
    console.log('Server has started on port! ',port)
})