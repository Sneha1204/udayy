## Design Schema
-------
* Table name - Subtraction
* Columns :-
    * Id int not null autoincrement
    * Minuend_Digits int not null Primary Key
    * Subtrahend_Digits int not null Primary Key
    * Minuend_Number int not null unique
    * Subtrahend_Number int not null unique
    * Correct_Answer int not null
    * Options varchar(150) not null

## Description 
----
* Minuend_Digits -> Hold the input value given by the user for number of digits in minuend 

* Subtrahend_Digits -> Hold the input value given by the user for number of digits in subtrahend 

* Minuend_Number -> A Random value is generated based on the corresponding value in Minuend_Digits using the Rand(). A unique constraint will ensure that the same random number isnt generated again

* Subtrahend_Number -> A Random value is generated based on the corresponding value in Minuend_Digits using the Rand(). A unique constraint will ensure that the same random number isnt generated again

* Correct_Answer - Holds the subtraction value of the corresponding Minuend_Number - Subtrahend_Number

* Options - We can perform the same logic for generating the options as done in the api using the Power(10,numOfDigits-1) where numOfDigits refers to the number of digits of the correct answer. 

## Logic
---
We will create a table initially and store all the values for every combination of minuend and subtrahend digits. **Example** -

| Minuend_Digits | Subtrahend_Digits |
| -------------- | ------------------|
|   1            |     1             |
|   2            |      1            |
| 3              |      1            |
| 4             |      1            |
| 5              |      1            |
| 6              |      1            |
| 7              |      1            |
| 8             |      1            |
| 9              |      1            |
| 2             |      2            |
| 3            |      2           |
 
 This will continue till all possible combinations are there where Minuend_Digits is always greater than Subtrahend_Digits.
 The case where the the values in Minuend_Digits = Subtrahend_Digits and the random number generated in Minuend_Number < Subtrahend_Number we can run this query at the time of table creation :-

### Query
```
case when Minuend_Digits = Subtrahend_Digits and Minuend_Number < Subtrahend_Number then Subtrahend_Number else Minuend_Number as Minuend_Number.
```

So we will have all the data generated from before and we can query the data based on user input.

## Additional Information
----
* We can also add an Index on Minuend_Digits to increase the speed of query search
* In case of some combinations are more frequent we can cache the results for further optimisation.
 


